import Http from './http';

export default class ContentService {
    static async fetch(data) {
        const content = await Http.get(`api/comments/comments?content_id=${data.contentId}&parent=${data.parent}&page=${data.page}`);
        return content;
    }
    static async save(comment) {
        const newComment = await Http.post('api/comments/comment', comment);
        return newComment;
    }
}

