/* global fetch FormData ENDPOINT */
import {isEmpty} from 'lodash';

const envPath = path;

export default class Http {
    static async get(url) {
        const response = await fetch(`${envPath}/${url}`, {
            method: 'get',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        });
        return response.json();
    }

    static async post(url, body) {
        const formData = new FormData();
        if (!isEmpty(body.images)) {
            body.images[0].forEach((file, i) => {
                formData.append(`${i}`, file);
            });
        }
        formData.append('content_id', body.contentId);
        formData.append('parent', body.parent);
        formData.append('content', body.content);
        const response = await fetch(`${envPath}/${url}`, {
            method: 'post',
            body: formData
        });
        return response.json();
    }

    static async put(url, body) {
        const response = await fetch(`${envPath}/${url}`, {
            method: 'put',
            credentials: 'same-origin',
            body: JSON.stringify(body),
            headers: {
                'content-type': 'application/json'
            }
        });
        return response.json();
    }

    static async delete(url, body) {
        const response = await fetch(`${envPath}/${url}`, {
            method: 'delete',
            credentials: 'same-origin',
            body: JSON.stringify(body),
            headers: {
                'content-type': 'application/json',
            }
        });
        return response.json();
    }
}
