import update from 'immutability-helper';
import {
    CONTENT_FETCH_REQUESTED,
    CONTENT_FETCH_SUCCEEDED,
    CHILDREN_FETCH_SUCCEEDED,
    COMMENT_SAVE_SUCCEEDED,
    REPLY_SAVE_SUCCEEDED,
    MORE_CONTENT_FETCH_SUCCEEDED,
    LIKE_SUCCEDED
} from '../actions';

const initialState = {
    content: {comments: []},
    commentCollapse: []
};

export default function content(state = initialState, action) {
    switch (action.type) {
        case CONTENT_FETCH_REQUESTED:
            return {...state, loading: true};
        case CONTENT_FETCH_SUCCEEDED:
            return {...state, saving: false, content: action.content};
        case MORE_CONTENT_FETCH_SUCCEEDED:
            return {...state, saving: false, content: [...state.content, ...action.content]};
        case COMMENT_SAVE_SUCCEEDED:
            const commentHelper = Object.assign({}, state.content);
            commentHelper.comments.unshift(action.content);
            return {...state, saving: false, content: commentHelper};
        case REPLY_SAVE_SUCCEEDED:
            if (state.content.comments[action.index].childrenComments !== undefined) {
                return update(state, {
                    content: {
                        comments: {
                            [action.index]: {
                                childrenComments: {
                                    $push: [action.content]
                                }
                            }
                        }
                    },
                    commentCollapse: {$set: {[action.index]: true}}
                });
            } else {
                return update(state, {
                    content: {
                        comments: {
                            [action.index]: {
                                childrenComments: {
                                    $set: [action.content]
                                }
                            }
                        }
                    },
                    commentCollapse: {$set: {[action.index]: true}}
                });
            }
        case CHILDREN_FETCH_SUCCEEDED:
            if (state.content.comments[action.index].childrenComments !== undefined) {
                return update(state, {
                    content: {
                        comments: {
                            [action.index]: {
                                childrenComments: {
                                    $push: action.content
                                }
                            }
                        }
                    },
                    commentCollapse: {$set: {[action.index]: true}}
                });
            } else {
                return update(state, {
                    content: {
                        comments: {
                            [action.index]: {
                                childrenComments: {
                                    $set: action.content
                                }
                            }
                        }
                    },
                    commentCollapse: {$set: {[action.index]: true}}
                });
            }
        case LIKE_SUCCEDED:
            return update(state, {
                content: {
                    comments: {
                        [action.index]: {
                            user_has_upvoted: {
                                $set: !state.content.comments[action.index].user_has_upvoted
                            }
                        }
                    }
                }
            });
        default:
            return state;
    }
}
