export const CONTENT_FETCH_REQUESTED = 'CONTENT_FETCH_REQUESTED';
export const MORE_CONTENT_FETCH_SUCCEEDED = 'MORE_CONTENT_FETCH_SUCCEEDED';
export const CONTENT_FETCH_SUCCEEDED = 'CONTENT_FETCH_SUCCEEDED';

export const requestContent = contentId => ({
    type: CONTENT_FETCH_REQUESTED,
    contentId
});

export const receiveContent = content => ({
    type: CONTENT_FETCH_SUCCEEDED,
    content
});

export const receiveMoreContent = content => ({
    type: MORE_CONTENT_FETCH_SUCCEEDED,
    content
});

export const COMMENT_SAVE_REQUESTED = 'COMMENT_SAVE_REQUESTED';
export const COMMENT_SAVE_SUCCEEDED = 'COMMENT_SAVE_SUCCEEDED';

export const requestSaveComment = comment => ({
    type: COMMENT_SAVE_REQUESTED,
    comment
});

export const notifyCommentSavedSuccessfully = content => ({
    type: COMMENT_SAVE_SUCCEEDED,
    content
});

export const REPLY_SAVE_SUCCEEDED = 'REPLY_SAVE_SUCCEEDED';

export const notifyReplySavedSuccessfully = (content, index) => ({
    type: REPLY_SAVE_SUCCEEDED,
    content,
    index
});

export const CHILDREN_FETCH_SUCCEEDED = 'CHILDREN_FETCH_SUCCEEDED';

export const receiveChildrenComments = (content, index) => ({
    type: CHILDREN_FETCH_SUCCEEDED,
    content,
    index
});

export const LIKE_REQUESTED = 'LIKE_REQUESTED';

export const requestLike = (content, index) => ({
    type: LIKE_REQUESTED,
    content,
    index
});

export const LIKE_SUCCEDED = 'LIKE_SUCCEDED';

export const likeSucceded = (content, index) => ({
    type: LIKE_SUCCEDED,
    content,
    index
});
