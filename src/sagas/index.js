import {all, takeEvery} from 'redux-saga/effects';
import {CONTENT_FETCH_REQUESTED, COMMENT_SAVE_REQUESTED, LIKE_REQUESTED} from '../actions';
import {ERROR_OCCURRED} from '../actions/common';

import {fetchContent, saveComment, likeRequested} from './content';
import handleError from './common';

export default function* root() {
    yield all([
        takeEvery(ERROR_OCCURRED, handleError),
        takeEvery(CONTENT_FETCH_REQUESTED, fetchContent),
        takeEvery(COMMENT_SAVE_REQUESTED, saveComment),
        takeEvery(LIKE_REQUESTED, likeRequested)
    ]);
}
