import {call, put} from 'redux-saga/effects';
import {isNull, isEmpty} from 'lodash';
import {
    receiveContent,
    notifyCommentSavedSuccessfully,
    receiveChildrenComments,
    notifyReplySavedSuccessfully,
    receiveMoreContent,
    likeSucceded
} from '../actions';
import {handleError} from '../actions/common';
import ContentService from '../services/content';

export function* fetchContent({contentId}) {
    try {
        const content = yield call(ContentService.fetch, contentId);
        if (!isEmpty(content.comments)) {
            if (isNull(content.comments[0].parent)) {
                yield put(receiveContent(content));
            } else {
                yield put(receiveChildrenComments(content.comments, contentId.index));
            }
        }
    } catch (err) {
        handleError(err);
    }
}

export function* saveComment({comment}) {
    try {
        const newComment = yield call(ContentService.save, comment);
        if (!isEmpty(newComment)) {
            if (isNull(newComment.parent)) {
                yield put(notifyCommentSavedSuccessfully(newComment));
            } else {
                yield put(notifyReplySavedSuccessfully(newComment, comment.commentIndex));
            }
        }
    } catch (err) {
        handleError(err);
    }
}

export function* likeRequested({contentId, index}) {
    try {
        yield put(likeSucceded(contentId, index));
    } catch (err) {
        handleError(err);
    }
}
