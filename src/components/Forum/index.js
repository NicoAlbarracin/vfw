/* global document */
/* eslint-disable max-len */
import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {connect} from 'react-redux';
import {
    Grid,
    Row,
    InputGroup,
    FormGroup,
    FormControl,
    Button,
    Collapse
} from 'react-bootstrap';
import {requestContent, requestSaveComment, requestLike} from '../../actions';

moment.locale('it');


class Forum extends Component {
    static propTypes = {
        requestContent: PropTypes.func.isRequired,
        requestSaveComment: PropTypes.func.isRequired,
        requestLike: PropTypes.func.isRequired
    };

    static defaultProps = {
        content: []
    };

    constructor(props) {
        super(props);
        this.state = {
            commentBody: '',
            replyCommentBody: '',
            childrenReplyCommentBody: '',
            page: 1,
            images: [],
            maxImages: [],
            reply: [],
            commentCollapse: [],
            // eslint-disable-next-line no-undef
            content_id: p_content_id,
            content: [],
            childReply: [],
            userLogged: user_logged
        };
    }

    componentDidMount() {
        this.props.requestContent({contentId: this.state.content_id, parent: 0, page: this.state.page});
    }

    componentDidUpdate(prevProps) {
        if (this.props.commentCollapse !== prevProps.commentCollapse) {
            this.setState(() => ({commentCollapse: this.props.commentCollapse}));
        }
    }

    handleChange(value, isReply, isChildReply) {
        if (isReply) {
            this.setState(() => ({replyCommentBody: value}));
        } else if (isChildReply) {
            this.setState(() => ({childrenReplyCommentBody: value}));
        } else {
            this.setState(() => ({commentBody: value}));
        }
    }

    toggleCollapse(commentIndex, contentId, fetch) {
        if (fetch && this.props.content.comments[commentIndex].childrenComments === undefined) {
            this.props.requestContent({
                contentId: this.state.content_id, parent: contentId, page: this.state.page, index: commentIndex
            });
        } else {
            this.setState(
                state => ({
                    commentCollapse: {
                        ...state.commentCollapse,
                        [commentIndex]: !this.state.commentCollapse[commentIndex]
                    }
                })
            );
        }
    }

    handleSubmit(parentId, comment, index) {
        this.props.requestSaveComment({
            content: comment,
            contentId: this.state.content_id,
            parent: parentId,
            images: this.state.images,
            commentIndex: index
        });
        this.setState({
            commentBody: '',
            replyCommentBody: '',
            childrenReplyCommentBody: '',
            images: [],
            temporaryImage: []
        });
        document.getElementById('formControlsFile').value = null;
    }

    handlePage(requestedPage) {
        this.setState(() => ({page: requestedPage}));
        this.props.requestContent({contentId: this.state.content_id, parent: 0, page: requestedPage});
    }

    handleFile(newFiles) {
        const files = Array.from(newFiles);
        this.setState(state => ({
            images: [...state.images, files]
        }));
        const temporaryUrl = files.map(image => URL.createObjectURL(image));
        this.setState({
            temporaryImage: temporaryUrl
        });
    }

    handleLike(commentIndex, contentId) {
        this.props.requestLike(contentId, commentIndex);
    }

    toggleReply(commentIndex) {
        this.setState(
            () => ({
                reply: []
            })
        );
        this.setState(
            state => ({
                reply: {...state.reply, [commentIndex]: !this.state.reply[commentIndex]}
            })
        );
    }

    showAllImages(commentIndex, value) {
        this.setState(
            state => ({
                maxImages: {...state.maxImages, [commentIndex]: value}
            })
        );
    }

    toggleChildReply(childIndex) {
        this.setState(
            () => ({
                childReply: []
            })
        );
        this.setState(
            state => ({
                childReply: {...state.childReply, [childIndex]: !this.state.childReply[childIndex]}
            })
        );
    }

    renderContent() {
        return (
            this.props.content.comments.map((comment, index) => (
                <Fragment key={comment.id}>
                    <div className="forum_single_row">
                        <div className="white_box_generic_user">

                            <div className="col-md-2 m-0 p-0">
                                <a href={`${comment.profile_url}`}>
                                    <div
                                        className="wbg_user_image"
                                        style={{backgroundImage: "url('/assets/img/gruppi/img-perfil.jpg')"}}
                                    >
                                        <img
                                            className="visibility_hidden"
                                            src="/assets/img/gruppi/img-perfil.jpg"
                                            alt=""
                                        />
                                    </div>
                                </a>
                            </div>

                            <div className="col-md-10 m-0 p-0">
                                <div className="wbg_user_data">
                                    <h1>
                                        <a href={`${comment.profile_url}`}>{comment.fullname}</a>
                                    </h1>
                                    <h2>{moment(comment.created).format('LLL')}</h2>
                                    <p>{comment.content}</p>
                                    <br/>
                                    {comment.attachments &&
                                    <div className="forum_gallery_image">
                                        {comment.attachments && comment.attachments.map((image, imageIndex) => (
                                            <Fragment key={image}>
                                                {(imageIndex < 3 || this.state.maxImages[index]) &&
                                                <div
                                                    className="forum_gallery_image_single"
                                                    style={{backgroundImage: `url(${image})`}}
                                                />}
                                            </Fragment>
                                        ))}
                                        {comment.attachments.length > 0 &&
                                        <div className="forum_gallery_image_single">
                                            <div className="box">
                                                <a className="btn_see_more btn_see_more_img_forum_gallery">
                                                    {this.state.maxImages[index] ?
                                                        <button onClick={() => this.showAllImages(index, false)}
                                                                className="hide_active">Nascondi tutto</button> :
                                                        <button onClick={() => this.showAllImages(index, true)}
                                                                className="hide_active">Vedi tutto</button>
                                                    }
                                                </a>
                                            </div>
                                        </div>}
                                    </div>}
                                    <div className="forum_footer">
                                        {comment.children_count > 0 &&

                                        <a className="text-semibold btn_see_more_respond">
                                            {this.state.commentCollapse[index] ?
                                                <span onClick={() => this.toggleCollapse(index, comment.id, false)}
                                                      className="show_active">Nascondi {comment.children_count} risposte</span> :
                                                <span onClick={() => this.toggleCollapse(index, comment.id, true)}
                                                      className="hide_active">Vedi {comment.children_count} risposte</span>
                                            }
                                        </a>}

                                        <ul className="menu_forum">
                                            <li className="menu_forum_like">
                                                <a onClick={() => this.handleLike(index, comment.id)}
                                                   className={comment.user_has_upvoted ? "linker-red icon_like" : "linker-red icon_unlike>"}
                                                >
                                                    INTERESSATI
                                                </a>
                                            </li>
                                            <li className="menu_forum_comment">
                                                <a
                                                    onClick={() => this.toggleReply(index)}
                                                    className="linker-red icon_comment"
                                                >
                                                    RISPONDERE
                                                </a>
                                            </li>
                                        </ul>

                                    </div>

                                </div>

                                <div className="col-md-12 border-separator m-0 p-0"/>

                            </div>
                        </div>
                    </div>
                    <Collapse in={this.state.reply[index]}>
                        <FormGroup>
                            <InputGroup>
                                <FormControl
                                    componentClass="textarea"
                                    value={this.state.replyCommentBody}
                                    onChange={e => this.handleChange(e.target.value, true)}
                                />
                                <InputGroup.Button>
                                    <Button
                                        disabled={this.state.replyCommentBody.length < 10}
                                        onClick={() => this.handleSubmit(comment.id, this.state.replyCommentBody, index)}
                                    >Rispondere
                                    </Button>
                                </InputGroup.Button>
                            </InputGroup>
                        </FormGroup>
                    </Collapse>
                    <Row>
                        {comment.childrenComments &&
                        <div>
                            <Collapse in={this.state.commentCollapse[index]}>
                                <div className="risposte-comentario">
                                    {comment.childrenComments ?
                                        this.props.content.comments[index].childrenComments.map((childComment, childIndex) => (
                                            <Fragment key={childComment.id}>
                                                <div className="forum_single_row">
                                                    <div className="white_box_generic_user">

                                                        <div className="col-md-2 m-0 p-0">
                                                            <div
                                                                className="wbg_user_image"
                                                                style={{backgroundImage: "url('/assets/img/gruppi/img-perfil.jpg')"}}
                                                            >
                                                                <img
                                                                    className="visibility_hidden"
                                                                    src="/assets/img/gruppi/img-perfil.jpg"
                                                                    alt=""
                                                                />
                                                            </div>
                                                        </div>

                                                        <div className="col-md-10 m-0 p-0">
                                                            <div className="wbg_user_data">
                                                                <h1>
                                                                    <a>{childComment.fullname}</a>
                                                                </h1>
                                                                <h2>{moment(childComment.created).format('LLL')}</h2>
                                                                <p>{childComment.content}</p>
                                                                <br/>
                                                                <div className="forum_footer">
                                                                    <ul className="menu_forum">
                                                                        <li className="menu_forum_like">
                                                                            <a onClick={() => this.handleLike(index, comment.id, comment.user_has_upvoted)}
                                                                               className="linker-red icon_like">
                                                                                INTERESSATI
                                                                            </a>
                                                                        </li>
                                                                        <li className="menu_forum_comment">
                                                                            <a
                                                                                onClick={() => this.toggleChildReply(childIndex)}
                                                                                className="linker-red icon_comment"
                                                                            >
                                                                                RISPONDERE
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>

                                                            </div>

                                                            <div className="col-md-12 border-separator m-0 p-0"/>

                                                        </div>
                                                    </div>
                                                </div>
                                                <Collapse in={this.state.childReply[childIndex]}>
                                                    <FormGroup>
                                                        <InputGroup>
                                                            <FormControl
                                                                type="text"
                                                                value={this.state.childrenReplyCommentBody}
                                                                onChange={e => this.handleChange(e.target.value, false, true)}
                                                            />
                                                            <InputGroup.Button>
                                                                <Button
                                                                    disabled={this.state.childrenReplyCommentBody.length < 10 || this.state.childrenReplyCommentBody.length > 120}
                                                                    onClick={() => this.handleSubmit(comment.id, this.state.childrenReplyCommentBody, index)}
                                                                >Submit
                                                                </Button>
                                                            </InputGroup.Button>
                                                        </InputGroup>
                                                    </FormGroup>
                                                </Collapse>
                                            </Fragment>))
                                        : <p>Loading...</p>}
                                </div>
                            </Collapse>
                        </div>}
                    </Row>
                </Fragment>))
        );
    }

    renderPagination() {
        const {paginate} = this.props.content;
        const active = paginate.current;
        const items = [];
        for (let number = 1; number <= paginate.last; number++) {
            items.push(
                <li key={number} className={number === active ? 'active' : ''}>
                    <a onClick={() => this.handlePage(number)}>{number}</a>
                </li>,
            );
        }

        return (
            <ul className="pagination pagination-grey">
                <ul className="pagination-left">
                    <li className={1 === paginate.current ? 'disabled' : ''}>
                        <a onClick={() => this.handlePage(1)}>
                            <i className="fa fa-angle-double-left" aria-hidden="true"/>
                        </a>
                    </li>
                    <li className={1 === paginate.current ? 'disabled' : ''}>
                        <a onClick={() => this.handlePage(paginate.previous)}>
                            <i className="fa fa-angle-left" aria-hidden="true"/>
                        </a>
                    </li>
                </ul>
                {items}
                <ul className="pagination-right">
                    <li className={paginate.last === paginate.current ? 'disabled' : ''}>
                        <a onClick={() => this.handlePage(paginate.next)}>
                            <i className="fa fa-angle-right" aria-hidden="true"/>

                        </a>
                    </li>
                    <li className={paginate.last === paginate.current ? 'disabled' : ''}>
                        <a onClick={() => this.handlePage(paginate.last)}>
                            <i className="fa fa-angle-double-right" aria-hidden="true"/>
                        </a>
                    </li>
                </ul>
            </ul>
        );
    }

    render() {
        return (
            <Fragment>
                {this.state.userLogged &&
                <Grid>
                    <FormGroup>
                        <Row className="post-input">
                            <FormControl
                                componentClass="textarea"
                                value={this.state.commentBody}
                                onChange={e => this.handleChange(e.target.value)}
                            />
                        </Row>
                        <Row>
                        </Row>
                        <Row className="post-button">
                            <Button
                                disabled={this.state.commentBody.length < 10}
                                onClick={() => this.handleSubmit(0, this.state.commentBody)}
                            >INVIARE
                            </Button>
                        </Row>
                        <FormControl
                            id="formControlsFile"
                            type="file"
                            label="File"
                            onChange={e => this.handleFile(e.target.files)}
                            multiple
                            className="inputFile"
                        />
                        {this.state.temporaryImage && this.state.temporaryImage.map(image => (
                            <img key={image} src={image} alt="" width={100} height={100}/>
                        ))}
                    </FormGroup>
                    {this.props.content.comments && this.renderContent()}
                    {this.props.content.paginate && this.renderPagination()}
                </Grid>}
            </Fragment>
        );
    }
}

export default connect(
    state => ({
        content: state.content.content,
        commentCollapse: state.content.commentCollapse
    }),
    dispatch => ({
        requestContent: contentId => dispatch(requestContent(contentId)),
        requestSaveComment: comment => dispatch(requestSaveComment(comment)),
        requestLike: (contentId, commentIndex) => dispatch(requestLike(contentId, commentIndex))
    })
)(Forum);
